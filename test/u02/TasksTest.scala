package u02

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test

class TasksTest {

  import Tasks._;

  @Test def testParity() = {
    assertEquals(lambdaParity(2), "even")
    assertEquals(lambdaParity(1), "odd")
    assertEquals(methodParity(2), "even")
    assertEquals(methodParity(1), "odd")
  }

  @Test def testEmpty() = {
    assert(lambdaNotEmpty("foo"));
    assert(!lambdaNotEmpty(""));
    assert(lambdaNotEmpty("foo") && !lambdaNotEmpty(""));
    assert(methodNotEmpty("foo"));
    assert(!methodNotEmpty(""));
    assert(methodNotEmpty("foo") && !methodNotEmpty(""));
    assert(methodGenericNotEmpty("foo"));
    assert(!methodGenericNotEmpty(""));
    assert(methodGenericNotEmpty("foo") && !methodNotEmpty(""));
  }

  @Test def testP() = {
    assert(p1(1)(2)(3))
    assert(p2(1, 2, 3))
    assert(p3(1)(2)(3))
    assert(p4(1, 2, 3))
    assert(!p1(3)(2)(1))
    assert(!p2(3, 2, 1))
    assert(!p3(3)(2)(1))
    assert(!p4(3, 2, 1))
  }

  @Test def testCompose() = {
    assertEquals(compose(_-1,_*2)(5), 9)
    assertEquals(genericCompose("eh già "+ _,"io sono ancora qua "+_)("eh già"), "eh già io sono ancora qua eh già")
  }

  @Test def testFib() = {
    assertEquals(fib(0), 0)
    assertEquals(fib(1), 1)
    assertEquals(fib(2), 1)
    assertEquals(fib(3), 2)
    assertEquals(fib(4), 3)
  }

  @Test def testShape() = {
    assertEquals(Shape.perimeter(Shape.Square(10)), 40)
    assertEquals(Shape.perimeter(Shape.Circle(3)), 2 * Math.PI * 3)
    assertEquals(Shape.perimeter(Shape.Rectangle(2, 2)), 8)
    assertEquals(Shape.area(Shape.Square(10)), 100)
    assertEquals(Shape.area(Shape.Circle(3)), Math.PI * 3 * 3)
    assertEquals(Shape.area(Shape.Rectangle(2, 2)), 4)
  }

  @Test def testOption() = {
    assertEquals(Option.filter(Option.Some(5))(_ > 2), Option.Some(5))
    assertEquals(Option.filter(Option.Some(5))(_ > 8), Option.None())
    assertEquals(Option.map(Option.Some(5))(_ > 2), Option.Some(true))
    assertEquals(Option.map(Option.None[Int])(_ > 2), Option.None())
    assertEquals(Option.map2(Option.Some(5))(Option.Some(10))(_ + _), Option.Some(15))
    assertEquals(Option.map2(Option.Some(5))(Option.Some("ok"))(_ + _), Option.Some("5ok"))
    assertEquals(Option.map2(Option.None[Int])(Option.Some(2))(_ + _ > 2), Option.None())
  }
}
