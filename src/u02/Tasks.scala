package u02

object Tasks {

  val lambdaParity : Int => String = x => x match {
    case n if n % 2 == 0 => "even"
    case _ => "odd"
  }

  def methodParity(x : Int): String = x match {
    case n if n % 2 == 0 => "even"
    case _ => "odd"
  }

  val lambdaNeg : (String => Boolean) => String => Boolean = x => !x(_);
  def methodGenericNeg[A](x: A => Boolean): A => Boolean = !x(_);
  def methodNeg(x: String => Boolean): String => Boolean = !x(_);

  val empty: String => Boolean = _=="" // predicate on strings
  val lambdaNotEmpty = lambdaNeg(empty) // which type of notEmpty?
  val methodNotEmpty = methodNeg(empty) // which type of notEmpty?
  val methodGenericNotEmpty = methodGenericNeg(empty) // which type of notEmpty?

  val p1: Int => Int => Int => Boolean = x => y => z => x <= y && y <= z
  val p2: (Int, Int, Int) => Boolean = (x, y, z) => x <= y && y <= z
  def p3(x: Int)(y: Int)(z: Int): Boolean = x <= y && y <= z
  def p4(x: Int, y: Int, z: Int): Boolean = x <= y && y <= z

  def genericCompose[A, B, C](f: B => C, g: A => B): A => C = x => f(g(x))
  def compose(f: Int => Int, g: Int => Int): Int => Int = x => f(g(x))

  def fib(n: Int): Int = {
    @annotation.tailrec
      def _fib(n: Int, prev: Int, current: Int) : Int = n match {
      case n if n <= 0 => current
      case _ => _fib(n - 1, prev = prev + current, current = prev) // tail because the result of the function call is the definitive result
      }
      _fib(n, 1, 0)
  }

  sealed trait Shape

  object Shape {
    case class Rectangle(l1: Double, l2: Double) extends Shape
    case class Circle(r: Double) extends Shape
    case class Square(l: Double) extends Shape

    def perimeter(shape: Shape) : Double = shape match {
      case Rectangle(l1, l2) => l1 * 2 + l2 * 2
      case Circle(r) => 2 * Math.PI * r
      case Square(l) => l * 4
    }

    def area(shape: Shape) : Double = shape match {
      case Rectangle(l1, l2) => l1 * l2
      case Circle(r) => Math.PI * r * r
      case Square(l) => l * l
    }
  }

  sealed trait Option[A]

  object Option {
    case class None[A]() extends Option[A]
    case class Some[A](a: A) extends Option[A]

    def isEmpty [A](opt : Option [A]) : Boolean = opt match {
      case None() => true
      case _ => false
    }

    def getOrElse [A, B >: A](opt : Option [A], orElse : B): B = opt match {
      case Some(a) => a
      case _ => orElse
    }

    def flatMap[A, B](opt : Option[A])(f: A => Option [B]) : Option [B] = opt match {
      case Some(a) => f(a)
      case _ => None()
    }

    def filter[A](opt : Option[A])(predicate: A => Boolean) : Option[A] = opt match {
      case Some(a) if predicate(a) => Some(a)
      case _ => None()
    }

    def map[A, B](opt : Option[A])(mapper: A => B) : Option[B] = opt match {
      case Some(a) => Some(mapper(a))
      case _ => None()
    }

    def map2[A, B, C](opt : Option[A])(opt2 : Option[B])(mapper: (A, B) => C) : Option[C] = opt match {
      case None() => None()
      case Some(a) => opt2 match {
        case None() => None()
        case Some(b) => Some(mapper(a, b))
        case _ => None()
      }
      case _ => None()
    }
  }
}
